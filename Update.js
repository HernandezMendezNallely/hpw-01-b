/*************VARIABLE GRUPO********/

var Grupo = function(   
  _clave,   
  _nombre, 
  _docente,
  _materias,
  _alumno
  ){   
    return {     
      "clave":_clave,     
      "nombre":_nombre,
      "docente":_docente,
      "materias":_materias,
      "alumnos":_alumno
    }   
  };


var Grupo1 = Grupo('ISA', 'Sistemas');
var Grupo2 = Grupo('ISB', 'Computacion');


/*************VARIABLE MATERIA********/

var Materias = function (   
  _clave,   
  _nombre 
  ){   
    return{     
      "clave":_clave,     
      "nombre":_nombre   
    };    
  };

var Materia1 = Materias ('AB34', 'Herramientas de Programacion Web'); 
var Materia2 = Materias ('DE56', 'Programacion Web');
var Materia3 = Materias ('MI14', 'Inteligencia Artificial'); 
var Materia4 = Materias ('GI78', 'Sistemas Programables');


/*************VARIABLE ALUMNOS********/
var Alumnos = function (   
  _clave,   
  _nombre,
  _apellidos,
  _calificaciones
  ){   
    return{     
      "clave":_clave,     
      "nombre":_nombre,
      "apellidos":_apellidos,
      "calificaciones":_calificaciones
    };    
  };
var Alumno1 = Alumnos ('1016', 'Nallely', 'Hernandez Mendez'); 
var Alumno2 = Alumnos ('1017', 'Francisco', 'Gomez Duran');
var Alumno3 = Alumnos ('1018', 'Fernando','Mendez Ramirez'); 
var Alumno4 = Alumnos ('1019', 'Erick', 'Aguilar Ramos');


/*************VARIABLE DOCENTES********/

 var Docentes = function (   
  _clave,   
  _nombre,
  _apellidos,
  _gradoAcademico
  ){   
    return{     
      "clave":_clave,     
      "nombre":_nombre,
      "apellidos":_apellidos,
      "Grado Academico":_gradoAcademico
    };    
  };
var Docente1 = Docentes ('ABC34', 'Antonio', 'Blas Hernandez', 'Maestria'); 
var Docente2 = Docentes('HES21', 'Patricia', 'Garia Robles','Doctorado');


/*************CONSTRUCCION DE FUNCIONES CREATE********/


/******FUNCION AGREGAR MATERIA*****/

function agregarMateriaAlGrupo(g,m){   
  if (!g.materias){   
    g.materias = [];                   
  }     
  if (!exiteMateriaEnGrupo (g, m.clave)){     
    g.materias.push(m);     
    return "La materia "+  "  " +m.nombre+ " " + "Fue agregada Exitosamente al Grupo";;   
  }   
  return "La materia "+  "  " +m.nombre+ " " + "ya existe en el grupo"; 
}  

function exiteMateriaEnGrupo(g,cm){   
  if(!g.materias && g.materias.length === 0){     
    return false;   
  }   
    for (var i=0; i<g.materias.length; i++){     
      if (g.materias [i].clave ===cm){       
        return true;     
      }   
    }   
  return false; }


agregarMateriaAlGrupo(Grupo1,Materia1);
agregarMateriaAlGrupo(Grupo1,Materia2);
agregarMateriaAlGrupo(Grupo2,Materia3);
agregarMateriaAlGrupo(Grupo2,Materia4);


/******FUNCION AGREGAR DOCENTE*****/

function agregarDocenteAlGrupo(g,d){   
  if (!g.docente){   
    g.docente = [];                   
  }     
  if (!exiteDocenteEnGrupo (g, d.clave)){     
    g.docente.push(d);     
    return "El docente "+  "  " +d.nombre+ "  " +d.apellido+ " " + "Fue Agregado Exitosamente al grupo";   
  }   
  return "El docente "+  "  " +d.nombre+ "  " +d.apellido+ " " + "ya existe en el grupo"; 
}  

function exiteDocenteEnGrupo(g,cd){   
  if(!g.docente && g.docente.length === 0){     
    return false;   
  }   
    for (var i=0; i<g.docente.length; i++){     
      if (g.docente [i].clave ===cd){       
        return true;     
      }   
    }   
  return false; }

agregarDocenteAlGrupo(Grupo1, Docente1);
agregarDocenteAlGrupo(Grupo2, Docente2);


/******FUNCION AGREGAR ALUMNO*****/


function agregarAlumnoAlGrupo(g,a){   
  if (!g.alumno){   
    g.alumno = [];                   
  }     
  if (!exiteAlumnoEnGrupo (g, a.clave)){     
    g.alumno.push(a);     
    return "El alumn@ "+  "  " +a.nombre+  "  " +a.apellidos+" " + "Fue Agregado Exitosamente al Grupo";   
  }   
  return "Lo sentimos el alumn@ "+  "  " +a.nombre+  "  " +a.apellidos+" " + "ya existe en el grupo"; 
}  

function exiteAlumnoEnGrupo(g,ca){   
  if(!g.alumno && g.alumno.length === 0){     
    return false;   
  }   
    for (var i=0; i<g.alumno.length; i++){     
      if (g.alumno [i].clave ===ca){       
        return true;     
      }   
    }   
  return false; }

agregarAlumnoAlGrupo(Grupo1, Alumno1);
agregarAlumnoAlGrupo(Grupo1, Alumno2);
agregarAlumnoAlGrupo(Grupo2, Alumno3);
agregarAlumnoAlGrupo(Grupo2, Alumno4);


/*************CONSTRUCCION DE FUNCIONES UPDATE********/


/********MODIFICAR MATERIA DEL GRUPO*******/

function ModificarMateriaDelGrupo(g,m,clv, nombre){   
 var pos;
 var text;
 var estado=false;
    for (var i=0; i<g.materias.length; i++){ 
        if (g.materias [i].clave ===m.clave){
            estado=true;
            pos=i;
        }
    }
  
   if (estado){
          g.materias [pos].clave =clv;
          g.materias [pos].nombre =nombre;
            text="Se modifico satisfactoriamente la materia";
    }else{
        text="La materia que intentas modificar no existe en el grupo"
    }
    return text;
}

ModificarMateriaDelGrupo(Grupo1, Materia1, 'AB34','Herramientas de Programacion Web');


/********MODIFICAR DOCENTE DEL GRUPO*******/

function ModificarDocenteDelGrupo(g,d,clv, nombre, ap,ga){   
 var pos;
 var text;
 var estado=false;
    for (var i=0; i<g.docente.length; i++){ 
        if (g.docente [i].clave ===d.clave){
            estado=true;
            pos=i;
        }
    }
  
   if (estado){
          g.docente [pos].clave =clv;
          g.docente [pos].nombre =nombre;
          g.docente [pos].apellidos =nombre;
          g.docente [pos].gradoAcademico =ga;
            text="Se modifico satisfactoriamente el docente";
    }else{
        text="El docente que intentas modificar no existe en el grupo" + " " + g.clave;
    }
    return text;
}

ModificarDocenteDelGrupo(Grupo2, Docente2,'HES21', 'Patricia', 'Garia Robles','Licenciatura');


/********MODIFICAR ALUMNO DEL GRUPO*******/

function ModificarAlumnoDelGrupo(g,a,clv, nombre, ap,c){   
 var pos;
 var text;
 var estado=false;
    for (var i=0; i<g.alumno.length; i++){ 
        if (g.alumno [i].clave ===a.clave){
            estado=true;
            pos=i;
        }
    }
  
   if (estado){
          g.alumno [pos].clave =clv;
          g.alumno [pos].nombre =nombre;
          g.alumno [pos].apellidos =ap;
          g.alumno [pos].calificaciones =c;
            text="Se modifico satisfactoriamente el alumno";
    }else{
        text="El alumno que intentas modificar no existe en el grupo" + " " + g.clave;
    }
    return text;
}

ModificarAlumnoDelGrupo(Grupo2, Alumno4,'1019', 'Erik', 'Aguilar Ramirez',undefined);



/*******MODIFICAR CALIFICACION AL ALUMNO********/
function ModificarCalificacionAlAlumno(a,m,c){   
 var pos;
 var text;
 var estado=false;
    for (var i=0; i<a.calificaciones.length; i++){ 
        if (a.calificaciones [i].materia ===m.nombre){
            estado=true;
            pos=i;
        }
    }
  
   if (estado){
          c.calificaciones [pos] =c;
            text="Se modifico satisfactoriamente el alumno";
    }else{
        text="El alumno que intentas modificar no existe en el grupo";
    }
    return text;
}

ModificarCalificacionAlAlumno(Alumno1,Materia2,"90");