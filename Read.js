/*************VARIABLE GRUPO********/

var Grupo = function(   
  _clave,   
  _nombre, 
  _docente,
  _materias,
  _alumno
  ){   
    return {     
      "clave":_clave,     
      "nombre":_nombre,
      "docente":_docente,
      "materias":_materias,
      "alumnos":_alumno
    }   
  };


var Grupo1 = Grupo('ISA', 'Sistemas');
var Grupo2 = Grupo('ISB', 'Computacion');


/*************VARIABLE MATERIA********/

var Materias = function (   
  _clave,   
  _nombre 
  ){   
    return{     
      "clave":_clave,     
      "nombre":_nombre   
    };    
  };

var Materia1 = Materias ('AB34', 'Herramientas de Programacion Web'); 
var Materia2 = Materias ('DE56', 'Programacion Web');
var Materia3 = Materias ('MI14', 'Inteligencia Artificial'); 
var Materia4 = Materias ('GI78', 'Sistemas Programables');


/*************VARIABLE ALUMNOS********/
var Alumnos = function (   
  _clave,   
  _nombre,
  _apellidos,
  _calificaciones
  ){   
    return{     
      "clave":_clave,     
      "nombre":_nombre,
      "apellidos":_apellidos,
      "calificaciones":_calificaciones
    };    
  };
var Alumno1 = Alumnos ('1016', 'Nallely', 'Hernandez Mendez'); 
var Alumno2 = Alumnos ('1017', 'Francisco', 'Gomez Duran');
var Alumno3 = Alumnos ('1018', 'Fernando','Mendez Ramirez'); 
var Alumno4 = Alumnos ('1019', 'Erick', 'Aguilar Ramos');


/*************VARIABLE DOCENTES********/

 var Docentes = function (   
  _clave,   
  _nombre,
  _apellidos,
  _gradoAcademico
  ){   
    return{     
      "clave":_clave,     
      "nombre":_nombre,
      "apellidos":_apellidos,
      "Grado Academico":_gradoAcademico
    };    
  };
var Docente1 = Docentes ('ABC34', 'Antonio', 'Blas Hernandez', 'Maestria'); 
var Docente2 = Docentes('HES21', 'Patricia', 'Garia Robles','Doctorado');


/*************CONSTRUCCION DE FUNCIONES CREATE********/


/******FUNCION AGREGAR MATERIA*****/

function agregarMateriaAlGrupo(g,m){   
  if (!g.materias){   
    g.materias = [];                   
  }     
  if (!exiteMateriaEnGrupo (g, m.clave)){     
    g.materias.push(m);     
    return "La materia "+  "  " +m.nombre+ " " + "Fue agregada Exitosamente al Grupo";;   
  }   
  return "La materia "+  "  " +m.nombre+ " " + "ya existe en el grupo"; 
}  

function exiteMateriaEnGrupo(g,cm){   
  if(!g.materias && g.materias.length === 0){     
    return false;   
  }   
    for (var i=0; i<g.materias.length; i++){     
      if (g.materias [i].clave ===cm){       
        return true;     
      }   
    }   
  return false; }


agregarMateriaAlGrupo(Grupo1,Materia1);
agregarMateriaAlGrupo(Grupo1,Materia2);
agregarMateriaAlGrupo(Grupo2,Materia3);
agregarMateriaAlGrupo(Grupo2,Materia4);


/******FUNCION AGREGAR DOCENTE*****/

function agregarDocenteAlGrupo(g,d){   
  if (!g.docente){   
    g.docente = [];                   
  }     
  if (!exiteDocenteEnGrupo (g, d.clave)){     
    g.docente.push(d);     
    return "El docente "+  "  " +d.nombre+ "  " +d.apellido+ " " + "Fue Agregado Exitosamente al grupo";   
  }   
  return "El docente "+  "  " +d.nombre+ "  " +d.apellido+ " " + "ya existe en el grupo"; 
}  

function exiteDocenteEnGrupo(g,cd){   
  if(!g.docente && g.docente.length === 0){     
    return false;   
  }   
    for (var i=0; i<g.docente.length; i++){     
      if (g.docente [i].clave ===cd){       
        return true;     
      }   
    }   
  return false; }

agregarDocenteAlGrupo(Grupo1, Docente1);
agregarDocenteAlGrupo(Grupo2, Docente2);


/******FUNCION AGREGAR ALUMNO*****/


function agregarAlumnoAlGrupo(g,a){   
  if (!g.alumno){   
    g.alumno = [];                   
  }     
  if (!exiteAlumnoEnGrupo (g, a.clave)){     
    g.alumno.push(a);     
    return "El alumn@ "+  "  " +a.nombre+  "  " +a.apellidos+" " + "Fue Agregado Exitosamente al Grupo";   
  }   
  return "Lo sentimos el alumn@ "+  "  " +a.nombre+  "  " +a.apellidos+" " + "ya existe en el grupo"; 
}  

function exiteAlumnoEnGrupo(g,ca){   
  if(!g.alumno && g.alumno.length === 0){     
    return false;   
  }   
    for (var i=0; i<g.alumno.length; i++){     
      if (g.alumno [i].clave ===ca){       
        return true;     
      }   
    }   
  return false; }

agregarAlumnoAlGrupo(Grupo1, Alumno1);
agregarAlumnoAlGrupo(Grupo1, Alumno2);
agregarAlumnoAlGrupo(Grupo2, Alumno3);
agregarAlumnoAlGrupo(Grupo2, Alumno4);


/*************CONSTRUCCION DE FUNCIONES READ********/


/*********FUNCION LISTA DE MATERIAS DEL GRUPO*****/

function LeerMateriaDelGrupo(g){   
 
    for (var i=0; i<g.materias.length; i++){ 
        
      console.log(g.materias [i].clave + " "+ g.materias [i].nombre);
    }
  return false;
  
}


LeerMateriaDelGrupo(Grupo1);



/*********FUNCION LISTA DE ALUMNOS POR GRUPO*****/

function LeerAlumnoDelGrupo(g){   
 
    for (var i=0; i<g.alumno.length; i++){ 
        
      console.log(g.alumno [i].clave + " "+ g.alumno [i].nombre+ " "+ g.alumno [i].apellidos);
    }
  return false;
  
}


LeerAlumnoDelGrupo(Grupo1);



/*********FUNCION LISTA DE ALUMNOS POR GRUPO*****/

function LeerDocenteDelGrupo(g){   
 
    for (var i=0; i<g.docente.length; i++){ 
        
      console.log(g.docente [i].clave + " "+ g.docente [i].nombre + " "+ g.docente [i].apellidos);
    }
  return false;
  
}


LeerDocenteDelGrupo(Grupo1);
